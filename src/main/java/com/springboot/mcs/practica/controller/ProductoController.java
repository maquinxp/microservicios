package com.springboot.mcs.practica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.mcs.practica.model.Producto;
import com.springboot.mcs.practica.service.impl.ProductoServiceImpl;

@RestController
@RequestMapping("/producto")
public class ProductoController {
	@Autowired
	private ProductoServiceImpl _productoService;
	
	@GetMapping(value = "/", produces = "application/json")	
	//public List<Producto> getAllPersonas(){
	public Map<String, Object> getAllPersonas(){
		Map<String, Object> respuesta = new HashMap<String, Object>();
		List<Producto> lsProducto = _productoService.getAllProductos();
		String error="0001";
		if(lsProducto==null) {
			error="0000";
		}
		respuesta.put("codigo_servicio", error);
		respuesta.put("producto", lsProducto);
		return respuesta;
		//return _productoService.getAllProductos();
	}
	
	@GetMapping(value = "/{id}", produces = "application/json")	
	public Map<String, Object> getProducto(@PathVariable ("id") Integer id){
		Map<String, Object> respuesta = new HashMap<String, Object>();
		Producto producto = _productoService.getProducto(id);
		String error="0001";
		if(producto==null) {
			error="0000";
		}
		respuesta.put("codigo_servicio", error);
		respuesta.put("producto", producto);
		return respuesta;
	}
	
	@PostMapping(value = "/", produces = "application/json")	
	public List<Producto> savePersona(@RequestBody Producto producto){
		
		_productoService.saveProducto(producto);
		
		return _productoService.getAllProductos();
	}	
	
	@DeleteMapping(value = "/{id}", produces = "application/json")	
	public List<Producto> deletePersona(@PathVariable ("id") Integer id){
		
		_productoService.deleteProducto(id);
		
		return _productoService.getAllProductos();
	}	
}
