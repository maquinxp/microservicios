package com.springboot.mcs.practica.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.mcs.practica.dao.impl.ProductoDaoImpl;
import com.springboot.mcs.practica.model.Producto;
import com.springboot.mcs.practica.service.ProductoService;

@Service
public class ProductoServiceImpl implements ProductoService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ProductoDaoImpl _productoDao;
	
	@Override
	public List<Producto> getAllProductos() {
		// TODO Auto-generated method stub
		return _productoDao.getAllProductos();
	}

	@Override
	public Producto getProducto(Integer id) {
		// TODO Auto-generated method stub
		return _productoDao.getProducto(id);
	}

	@Override
	public void saveProducto(Producto producto) {
		try {
			_productoDao.saveProducto(producto);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}	
		
	}

	@Override
	public void deleteProducto(Integer id) {
		try {
			_productoDao.deleteProducto(id);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

}
