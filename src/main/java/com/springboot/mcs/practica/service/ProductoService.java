package com.springboot.mcs.practica.service;

import java.util.List;

import com.springboot.mcs.practica.model.Producto;

public interface ProductoService {
	List<Producto> getAllProductos();
	Producto getProducto(Integer id);
	void saveProducto(Producto producto);
	void deleteProducto(Integer id);
}
