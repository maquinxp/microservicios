package com.springboot.mcs.practica.dao;

import java.util.List;

import com.springboot.mcs.practica.model.Producto;


public interface ProductoDao {
	List<Producto> getAllProductos();
	Producto getProducto(Integer id);
	void saveProducto(Producto producto);
	void deleteProducto(Integer id);
}
