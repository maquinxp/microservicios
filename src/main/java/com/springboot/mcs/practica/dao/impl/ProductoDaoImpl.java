package com.springboot.mcs.practica.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.springboot.mcs.practica.dao.ProductoDao;
import com.springboot.mcs.practica.model.Producto;
import com.springboot.mcs.practica.rowmapper.ProductoRowMapper;

@Repository
public class ProductoDaoImpl extends JdbcDaoSupport implements ProductoDao {

	public ProductoDaoImpl(DataSource dataSource) {
		this.setDataSource(dataSource);
	}
	
	@Override
	public List<Producto> getAllProductos() {
List<Producto> listaProductos = new ArrayList<Producto>();
		
		String sql = " SELECT id, descripcion, categoria, precio_unitario, stock_actual, stock_minimo, estado\n" + 
				" FROM microservicios.producto";
		
		try {
			
			RowMapper<Producto> productoRow = new ProductoRowMapper();
			listaProductos = getJdbcTemplate().query(sql, productoRow);
			logger.debug("Se han listado "+listaProductos.size()+" productos");
					
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return listaProductos;
	}

	@Override
	public Producto getProducto(Integer id) {
		Producto producto = new Producto();	
		List<Producto> listaProducto = new ArrayList<Producto>();
		
		String sql = " SELECT id, descripcion, categoria, precio_unitario, stock_actual, stock_minimo, estado\n" + 
				" FROM microservicios.producto where id='"+id+"'";
				
		try {
			
			RowMapper<Producto> productoRow = new ProductoRowMapper();
			listaProducto = getJdbcTemplate().query(sql, productoRow);
			
			producto = listaProducto.get(0);
			
			logger.debug("Se ha traido el producto "+listaProducto.get(0).toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return producto;
	}

	@Override
	public void saveProducto(Producto producto) {
		String sql = "insert into microservicios.producto (descripcion, categoria, precio_unitario, stock_actual, stock_minimo, estado) "  
				+ "values (?, ?, ?, ?, ?, ?);";
		
		Object[] params = { producto.getDescripcion(), producto.getCategoria(), producto.getPrecio_unitario(), producto.getStock_actual(), producto.getStock_minimo(), producto.getEstado()};
		int[] tipos = { Types.VARCHAR, Types.VARCHAR, Types.LONGNVARCHAR, Types.INTEGER, Types.INTEGER, Types.CHAR};
		
		try {
			
			int filas = getJdbcTemplate().update(sql, params,tipos);
			
			logger.debug("Se han insertado : "+filas+" filas");
			logger.debug("Se ha registrado a la persona "+producto.toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
	}

	@Override
	public void deleteProducto(Integer id) {
		int regeliminados = 0;		
		String sql = " delete from microservicio.producto where id ='"+id+"'";		
		try {			
			regeliminados = getJdbcTemplate().update(sql);
			logger.debug("Se han eliminado "+regeliminados+" producto con id = "+id);
		} catch (Exception e) {			
			logger.error(e.getMessage());
		}
		
	}

}
